<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class PokemonController extends Controller
{
    /**
     * Show pokemons
     */
    public function showPokemons()
    {
        $client = new Client;
        $request = $client->get('https://pokeapi.co/api/v2/pokemon/?offset=0&limit=150');
        $response = $request->getBody();
        return $response;

    }

    public function showPokemon(string $id)
    {
        $client = new Client;
        $request = $client->get('https://pokeapi.co/api/v2/pokemon/'.$id.'/');
        $response = json_decode($request->getBody());
        
        $json = '
   
              {
                "name": '.json_encode($response->name).',
                "moves": '.json_encode($response->moves).',
                "abilities": '.json_encode($response->abilities).',
                "stats": '.json_encode($response->stats).'
              },

          ';
        
        

        return $json;

    }
}
